package main

import (
    "fmt"
    "math"
    "sort"
    "strings"
    "time"
)

const (
    longestWord     = 28
    chMax           = 26
    chOffset        = 97
    chCapitalOffset = 65
    refBits         = 0b00011111
    wordBit         = 0b00100000
    foundBit        = 0b01000000
    deletedBit      = 0b10000000
    deletedFoundBit = foundBit | deletedBit
    closedBit       = wordBit | foundBit
)

var (
    nodeBuffer  [426852]Node
    root                          = &Node{}
    wordBuffer  []string          = nil
)

type Node struct {
    data     byte // 01234567, bit 0-4 = references, bit 5 = word, bit 6 = found, bit 7 = deleted
    contents [chMax]*Node
}

// Only run on single thread
func incrementRefs(node *Node) {
    if node.data & refBits < chMax {
        node.data++
    } else {
        panic(fmt.Sprintf("Data overflowed past ref counter! %d", node.data))
    }
}

func load() {
    start := time.Now()

    n := len(wordlist)
    var (
        scanBuffer  [longestWord]byte
        nodeCache   [longestWord + 1]*Node
        runeCounts  [chMax]int
        runeWeights [chMax]float64
    )

    nodeCache[0] = root
    i := 0               // Index of current word
    previousLength := -2 // Length of previous word
    nodeIndex := 0       // Next node from the node buffer
    indexed   := 0       // How many words were indexed
    p := root

    for c := 0; c < n; c++ {
        ch := wordlist[c] - chOffset
        if ch < chMax && i < longestWord {
            runeCounts[ch]++
            if scanBuffer[i] == ch {
                if i < previousLength {
                    i++
                    continue
                }
            } else {
                scanBuffer[i] = ch
            }

            i++

            if previousLength != -1 {
                previousLength = -1
                p = nodeCache[i - 1]
            }

            q := &nodeBuffer[nodeIndex]

            nodeIndex++
            p.contents[ch] = q
            nodeCache[i] = q
            incrementRefs(p)

            p = q
        } else if i > 0 {
            p.data |= wordBit

            indexed++
            previousLength = i
            i = 0
        }
    }

    total := float64(0)
    for i := 0; i < len(runeCounts); i++ {
        total += float64(runeCounts[i])
    }

    for i := 0; i < len(runeCounts); i++ {
        runeWeights[i] = float64(runeCounts[i]) / total
    }

    loadWeights(runeWeights)

    fmt.Printf("Indexed %d word(s) in %v! Longest word %d!\n", indexed, time.Now().Sub(start), longestWord)
}

func byteConv(buffer [longestWord]byte, start, end int) string {
    bytes := buffer[start:end]
    for i, _ := range bytes {
        bytes[i] = bytes[i] + chOffset
    }
    return string(bytes)
}

func dfsWalkThrough(word *[longestWord]byte, end int, current *Node) {
    if current.data & closedBit == closedBit {
        wordBuffer = append(wordBuffer, string(word[0:end]))
    } else if current.data & wordBit == wordBit {
        return
    }

    for i, n := range current.contents {
        if n != nil {
            word[end] = byte(i + chOffset)
            dfsWalkThrough(word, end + 1, n)
        }
    }
}

func walkThrough() {
    type State struct {
        node *Node
        index byte
    }

    var sp byte = 0
    var buffer [longestWord]byte
    var stack [longestWord + 1]State
    stack[0] = State{node: root, index: 0}

loop:
    if sp <= longestWord {
        state := stack[sp]

    skip1:
        index := state.index
        if index >= chMax {
            sp--
            goto loop
        }
        p := state.node

    skip2:
        q := p.contents[index]
        for q == nil {
            index++
            if index < chMax {
                q = p.contents[index]
            } else {
                sp--
                goto loop
            }
        }

        if q.data & foundBit == 0 {
            if q.data & wordBit == wordBit {
                if index >= 25 {
                    sp--
                    goto loop
                } else {
                    index++
                    goto skip2
                }
            } else {
                buffer[sp] = index + chOffset
            }
        } else {
            buffer[sp] = index + chOffset
            wordBuffer = append(wordBuffer, string(buffer[0:sp + 1]))
        }

        state.index = index + 1
        stack[sp] = state
        sp++
        state = State{node: q, index: 0}
        goto skip1
    }
}

func printFound() {
    wordBuffer = make([]string, 0, 168845)
    WalkStart := time.Now()
    // dfsWalkThrough(&[longestWord]byte{}, 0, root)
    walkThrough()
    walkTime := time.Now().Sub(WalkStart)
    var builder strings.Builder

    if len(wordBuffer) == 0 {
        builder.WriteString(fmt.Sprintf("\nWalk took %f second(s)\n", float64(walkTime) / 1000))
        builder.WriteString("No words were found.\n")
    } else {
        sort.Slice(wordBuffer, func(i, j int) bool {
            a := wordBuffer[i]
            b := wordBuffer[j]

            if len(a) == len(b) {
                return a < b
            } else {
                return len(a) < len(b)
            }
        })

        wordIndex := len(wordBuffer)
        length := 0
        for i := int(math.Max(0, float64(wordIndex - maxOutput))); i < wordIndex; i++ {
            word := wordBuffer[i]
            l := len(word)

            if l > length {
                length = l
                builder.WriteString(fmt.Sprintf("\n%d Letter Words:\n\n", length))
            }

            builder.WriteString(word)
            builder.WriteByte(10)
        }

        builder.WriteString(fmt.Sprintf("\nWalk took %v.\n", walkTime))
        builder.WriteString(fmt.Sprintf("%d words were found.\n", wordIndex))
    }

    fmt.Print(builder.String())
    wordBuffer = nil
}
