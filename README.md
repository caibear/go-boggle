**Abstract**

This project is about optimizing a boggle word search algorithm.
Given a *n* by *n* board of letters find all the words that can be formed by 
starting at any letter and only moving to adjacent letters given a list of words.

**How to build**

Step 1
Download go, if you already have it skip this step.

Step 2
Run `go build` in the root directory of the project.

Step 3
Run the executable `./boggle`.

**How to use the profiler**

Step 1
Build the project, see **How to build**.

Step 2
Run the executable with `-cpuprofile cpu.prof -memprofile mem.prof` arguments.

Step 3
Once the program has finished executing, run either `go tool pprof cpu.prof`
or `go tool pprof -http=:8000 cpu.prof` if you want the web gui.

